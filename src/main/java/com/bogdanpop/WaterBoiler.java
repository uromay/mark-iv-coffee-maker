package com.bogdanpop;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ppop on 21.12.2016.
 */
public class WaterBoiler implements Observer {
    Hardware hardware;

    public WaterBoiler(Hardware hardware) {
        this.hardware = hardware;
    }

    @Override
    public void update(Observable observable, Object o) {
        Sensor sensor = (Sensor) o;
        if (sensor.getName().equals("boiler")) {
            hardware.setBoilerStatus(sensor.getStatus());
        }
        if (sensor.getName().equals("button")) {
            hardware.setButtonStatus(sensor.getStatus());
        }
    }
}
