package com.bogdanpop;

/**
 * Created by ppop on 22.12.2016.
 */
public class Sensor {
    private String name;
    private int status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Sensor() {
    }

    public Sensor(int status) {
        this.status = status;
    }

    public Sensor(String name) {
        this.name = name;
    }

    public Sensor(String name, int status) {
        this.name = name;
        this.status = status;
    }
}
