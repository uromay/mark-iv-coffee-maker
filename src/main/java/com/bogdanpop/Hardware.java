package com.bogdanpop;

/**
 * Created by ppop on 22.12.2016.
 */
public class Hardware {
    private int boilerStatus;
    private int warmerPlateStatus;
    private int buttonStatus;

    public Hardware(int boilerStatus, int warmerPlateStatus, int buttonStatus) {
        this.boilerStatus = boilerStatus;
        this.warmerPlateStatus = warmerPlateStatus;
        this.buttonStatus = buttonStatus;
    }

    public int getBoilerStatus() {
        return boilerStatus;
    }

    public void setBoilerStatus(int boilerStatus) {
        this.boilerStatus = boilerStatus;
    }

    public int getWarmerPlateStatus() {
        return warmerPlateStatus;
    }

    public void setWarmerPlateStatus(int warmerPlateStatus) {
        this.warmerPlateStatus = warmerPlateStatus;
    }

    public int getButtonStatus() {
        return buttonStatus;
    }

    public void setButtonStatus(int buttonStatus) {
        this.buttonStatus = buttonStatus;
    }
}
