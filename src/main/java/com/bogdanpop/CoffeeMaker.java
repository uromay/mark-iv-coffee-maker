package com.bogdanpop;

import static org.mockito.Mockito.mock;

/**
 * Created by ppop on 21.12.2016.
 */
public class CoffeeMaker {
    private CoffeeMakerAPI api;
    private WarmerPlate warmerPlate;
    private WaterBoiler waterBoiler;
    private UserControls userControls;
    private Hardware hardware;

    public CoffeeMaker() {
        api = mock(CoffeeMakerAPI.class);
        hardware = new Hardware(CoffeeMakerAPI.BOILER_NOT_EMPTY, CoffeeMakerAPI.POT_EMPTY, CoffeeMakerAPI.BREW_BUTTON_NOT_PUSHED);
        warmerPlate = new WarmerPlate(hardware);
        waterBoiler = new WaterBoiler(hardware);
        userControls = new UserControls(hardware);
    }

    public void startMachine() {
        WarmerPlateEvents warmerPlateEvents = new WarmerPlateEvents(CoffeeMakerAPI.POT_EMPTY);
        BoilerEvents boilerEvents = new BoilerEvents(CoffeeMakerAPI.BOILER_NOT_EMPTY);
        ButtonEvents buttonEvents = new ButtonEvents(CoffeeMakerAPI.BREW_BUTTON_NOT_PUSHED);

        warmerPlateEvents.addObserver(warmerPlate);
        boilerEvents.addObserver(waterBoiler);
        boilerEvents.addObserver(userControls);
        buttonEvents.addObserver(waterBoiler);
        buttonEvents.addObserver(userControls);

        while (true) {
            // user pushes button
            if (hardware.getButtonStatus() == CoffeeMakerAPI.BREW_BUTTON_PUSHED) {
                // conditions are met to start brewing
                if (hardware.getBoilerStatus() == CoffeeMakerAPI.BOILER_NOT_EMPTY &&
                        hardware.getWarmerPlateStatus() == CoffeeMakerAPI.POT_EMPTY) {
                    api.setBoilerState(CoffeeMakerAPI.BOILER_ON);
                    api.setReliefValveState(CoffeeMakerAPI.VALVE_CLOSED);
                    api.setWarmerState(CoffeeMakerAPI.WARMER_ON);
                    api.setIndicatorState(CoffeeMakerAPI.INDICATOR_OFF);
                }
            }
            // brewing process finishes
            if (hardware.getBoilerStatus() == CoffeeMakerAPI.BOILER_EMPTY) {
                api.setBoilerState(CoffeeMakerAPI.BOILER_OFF);
                api.setReliefValveState(CoffeeMakerAPI.VALVE_OPEN);
                api.setWarmerState(CoffeeMakerAPI.WARMER_ON);
                api.setIndicatorState(CoffeeMakerAPI.INDICATOR_ON);
            }
            // user removes coffee pot from warmer plate
            if (hardware.getWarmerPlateStatus() == CoffeeMakerAPI.WARMER_EMPTY) {
                api.setBoilerState(CoffeeMakerAPI.BOILER_OFF);
                api.setReliefValveState(CoffeeMakerAPI.VALVE_OPEN);
                api.setWarmerState(CoffeeMakerAPI.WARMER_OFF);
                api.setIndicatorState(CoffeeMakerAPI.INDICATOR_OFF);
            }
            // user returns the coffee pot with coffee on the warmer plate
            if (hardware.getWarmerPlateStatus() == CoffeeMakerAPI.POT_NOT_EMPTY) {
                api.setIndicatorState(CoffeeMakerAPI.INDICATOR_ON);
                api.setWarmerState(CoffeeMakerAPI.WARMER_ON);
            }
            // user returns the empty coffee pot on the warmer plate
            if (hardware.getWarmerPlateStatus() == CoffeeMakerAPI.POT_EMPTY) {
                api.setIndicatorState(CoffeeMakerAPI.INDICATOR_OFF);
                api.setWarmerState(CoffeeMakerAPI.WARMER_OFF);
            }
        }
    }

}
