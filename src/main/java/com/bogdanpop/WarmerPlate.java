package com.bogdanpop;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ppop on 21.12.2016.
 */
public class WarmerPlate implements Observer {
    Hardware hardware;

    public WarmerPlate(Hardware hardware) {
        this.hardware = hardware;
    }

    @Override
    public void update(Observable observable, Object o) {
        Sensor sensor = (Sensor) o;
        if (sensor.getName().equals("warmerPlate")) {
            System.out.println("New value of " + sensor.getName() + " is: " + sensor.getStatus());
            hardware.setWarmerPlateStatus(sensor.getStatus());
        }
    }
}
