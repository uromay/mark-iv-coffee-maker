package com.bogdanpop;

import java.util.Observable;

/**
 * Created by ppop on 22.12.2016.
 */
public abstract class EventsObservable extends Observable {
    public Sensor sensor;

    public void setValue(int status) {
        if (this.sensor.getStatus() != status) {
            this.sensor.setStatus(status);
            setChanged();
            notifyObservers(sensor);
        }
    }
}
