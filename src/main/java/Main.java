import com.bogdanpop.CoffeeMaker;

public class Main {

    public static void main(String[] args) {
        CoffeeMaker coffeeMaker = new CoffeeMaker();
        coffeeMaker.startMachine();
    }
}
